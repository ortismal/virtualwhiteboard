import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import Post from 'backend/models'

const app = express();
const router = express.Router();
const connection = mongoose.connection;
const db = 'mongodb://localhost:27017/posts'

app.use(cors());
app.use(bodyParser.json());

//Unified Topology added because of deprecation issue
mongoose.set('useUnifiedTopology', true);

// useNewUrlParser is added to the connection because of deprecation issues
mongoose.connect(db, {useNewUrlParser: true});
connection.once('open', () => {
    console.log('MongoDB database connection established.');
});

app.use('/', router);
app.listen(4000, () => console.log(`Express server running on port 4000`));

// Return all posts
router.route('/frontpage').get((req, res) => {
    Post.find((err, posts) => {
        if (err)
            console.log(err);
        else
            res.json(posts);
    });
});

// Return a post based on its ID
router.route('/frontpage/:id').get((req, res) => {
    Post.findById(req.params.id, (err, post) => {
        if (err)
            console.log(err);
        else
            res.json(posts);
    });
});

// Adds a new post
router.route('/posts/add').post((req, res) => {
    let post = new Post(req.body);
    post.save()
        .then(post => {
            res.status(200).json({'post': 'Added successfully'});
        })
        .catch(err => {
            res.status(400).send('Failed to create new post');
        });
});

// Update user by ID
router.route('/posts/update/:id').post((req, res) => {
    Post.findById(req.params.id, (err, post) => {
        if (!post)

            return next(new Error('Could not load post'));
        
        else {

            post.title = req.body.title;
            post.text = req.body.text;

            post.save().then(post => {
                res.json('Post updated!');
            }).catch(err => {
                res.status(400).send('Update failed.');
            });
        }
    });
});

// Delete post by ID
router.route('/posts/delete/:id').get((req, res) => {
    Post.findByIdAndRemove({_id: req.params.id}, (err, post) => {
        if (err)
            res.json(err);
        else
            res.json('Post removed successfully!');
    });
});