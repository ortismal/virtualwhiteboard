# Virtual Whiteboard

Et virtuelt whiteboard, designet til at kunne poste korte tekstbeskeder, samt uploade billeder og youtube-videoer.


## Udviklingsværktøjer

Jeg har for dette projekt valgt at udvikle en fullstack applikation med MEAN-stacken.
Denne stack har flere fordele, blandt andet:

* Både server og klienten er udviklet i JavaScript. Dette nedsætter behovet for at omstille sig sprogligt løbende, og giver sproglig kontinuitet i stacken.
* MongoDB gemmer documents i JSON-like format. Dette fjerner behovet for at konvertere queries fra databasen til frontenden.
* Angular (version 9 i skrivende stund) er et web application framework der bruges til SPA's. SPA's giver øget responstid på webapplikationen, da alle ressourcer bliver hentet når siden bliver indlæst, eller efter behov.

## Installationsguide:

1. Clone repository
2. Installér Node.js: https://nodejs.org/
3. Installér Angular CLI'et: ` npm install -g @angular/cli`
4. Installér MongoDB: https://docs.mongodb.com/manual/installation/ (husk at starte mongod service, og dobbelttjek evt. ved sudo service mongod status).
5. Start server: `cd ~/virtualwhiteboard/backend && npm run dev`
6. Åben ny terminal og serve angular applikationen: `~/virtualwhiteboard/frontend && ng serve -o`
