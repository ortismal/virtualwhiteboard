import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FrontpageComponent } from './frontpage/frontpage.component';
import { EditpostComponent } from './editpost/editpost.component';
import { CreatepostComponent } from './createpost/createpost.component';
import { LoginComponent } from './login/login.component';
import { CreateuserComponent } from './createuser/createuser.component';
import { EdituserComponent } from './edituser/edituser.component';
import { DeleteuserComponent } from './deleteuser/deleteuser.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatTableModule } from '@angular/material/table';
import { MatDividerModule } from '@angular/material/divider';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { HttpClientModule } from '@angular/common/http';
import { MatOptionModule } from '@angular/material/core';
import { RouterModule, Routes} from '@angular/router';
import { PostService } from './post.service';
import { LayoutModule } from '@angular/cdk/layout';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';


const routes: Routes = [
  { path: 'frontpage', component: FrontpageComponent },
  { path: 'createpost', component: CreatepostComponent },
  { path: 'editpost', component: EditpostComponent },
  { path: 'login', component: LoginComponent },
  { path: 'createuser', component: CreateuserComponent },
  { path: 'edituser', component: EdituserComponent },
  { path: 'deleteuser', component: DeleteuserComponent },
  { path: '', redirectTo: '/login', pathMatch: 'full'}
];


@NgModule({
  declarations: [
    AppComponent,
    FrontpageComponent,
    EditpostComponent,
    CreatepostComponent,
    LoginComponent,
    CreateuserComponent,
    EdituserComponent,
    DeleteuserComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatInputModule,
    MatOptionModule,
    MatSelectModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatTableModule,
    MatDividerModule,
    MatSnackBarModule,
    RouterModule.forRoot(routes),
    LayoutModule,
    MatSidenavModule,
    MatListModule
  ],
  providers: [PostService],
  bootstrap: [AppComponent]
})
export class AppModule { }
