import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private http: HttpClient) { }

  uri = 'http://localhost:4000';

  getPosts() {
    return this.http.get(`${this.uri}/frontpage`);
  }

  getPostById(id: any) {
    return this.http.get(`${this.uri}/frontpage/${id}`);
  }

  createPost(title, text) {
    const post = {
      title,
      text
    };
    return this.http.post(`${this.uri}/createpost`, post)
  }

  updatePost(id, title, text) {
    const post = {
      title,
      text
    };
    return this.http.post(`${this.uri}/updatepost/${id}`, post);
  }

  deletePost(id) {
    return this.http.get(`${this.uri}/deletepost/${id}`);
  }
}
