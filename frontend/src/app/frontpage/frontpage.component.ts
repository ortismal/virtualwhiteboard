import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Post } from '../../../../backend/models/post.model';
import { PostService } from '../post.service';

@Component({
  selector: 'app-frontpage',
  templateUrl: './frontpage.component.html',
  styleUrls: ['./frontpage.component.scss']
})
export class FrontpageComponent implements OnInit {

  posts: Post[];

  constructor(private postService: PostService, private router: Router) { }

  ngOnInit() {
    this.fetchPosts();
  }

  fetchPosts() {
    this.postService.getPosts().subscribe({
      next: (data: Post[]) => {
        this.posts = data;
      },
      error: (err: any) => {
        console.log('Posts cannot be fetched');
      }
    });
  }

  editPost(id: any) {
    this.router.navigate([`/edit/${id}`]);
  }

  deletePost(id) {
    this.postService.deletePost(id).subscribe(() => {
      this.fetchPosts();
    });
  }
}
